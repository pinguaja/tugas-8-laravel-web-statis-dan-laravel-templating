<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    /*tugas release 1
    public function form(){
        return view('halaman.register');
    }
    public function signUp(Request $request){
            // dd($request -> all());
            $namaDepan = $request['nama_depan'];
            $namaBelakang = $request['nama_belakang'];
            return view('halaman.welcome', compact('namaDepan', 'namaBelakang'));
    }
    */
    public function form(){
        return view('register');
    }
    public function signUp(Request $request){
        // dd($request -> all());
        $namaDepan = $request['nama_depan'];
        $namaBelakang = $request['nama_belakang'];
        return view('welcome', compact('namaDepan', 'namaBelakang'));
    }
}
