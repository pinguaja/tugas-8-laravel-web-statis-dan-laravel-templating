@extends('layout.master')
@section('judul')
Halaman Tambah Pemeran
@endsection

@section('content')
    <form method="post" action="/cast">
        @csrf
        {{-- nama --}}
        <div class="form-group">
            <label >Nama Pemeran</label>
            <input type="text" name="nama" class="form-control" >
        </div>
        {{-- untuk memunculkan alert jika validasi salah --}}
        @error('nama') 
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        {{-- umur --}}
        <div class="form-group">
            <label >Umur Pemeran</label>
            <input type="number" name="umur" class="form-control" >
        </div>
        {{-- untuk memunculkan alert jika validasi salah --}}
        @error('umur') 
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        {{-- bio --}}
        <div class="form-group">
            <label>Biodata</label>
            <textarea name="bio" cols="30" rows="10" class="form-control"></textarea>        
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
        @error('bio')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </form>
@endsection